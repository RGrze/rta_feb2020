import os
from configparser import ConfigParser
import time
from datetime import datetime

from thermocouples_reference import thermocouples
from PyQt5.QtCore import QObject, QTimer, pyqtSlot, pyqtSignal
from scipy import interpolate

from instruments import pid
from instruments import psu
from instruments import adafruit
from instruments import daqhat


class Worker(QObject):
    """Worker object that handles operation loop"""
    finished = pyqtSignal()
    plot_update = pyqtSignal()

    def __init__(self, recipe):
        super(Worker, self).__init__()
        self.recipe = recipe
        self.config = self.get_config()
        self.adc_type = self.config.get('DEFAULT', 'adc').lower()
        self.stop_flag = False

        self.x = []
        self.y = []

    @pyqtSlot()
    def run(self):
        """Function called when QThread has started.
        It initializes all required stuff to perform operation,
        creates new logfile and then starts the main loop
        """
        self.target_temp = interpolate.interp1d(self.recipe.time,
                                                self.recipe.temperature)

        self.initialize_instruments()

        self.filename = self.get_filename()

        headers = ['time', 'target_temp', 'real_temp',
                   'psu_voltage', 'psu_current'
                   ]
        self.write_to_file(self.filename, "\t".join(headers), 'w')

        self.timer = self.initialize_loop_timer()

        self.start_time = time.time()
        self.finish_time = self.recipe.time[-1]
        self.timer.start()

    def main_loop(self):
        """This function is constantly called by QTimer with set interval.
        """
        time_elapsed = time.time() - self.start_time
        if time_elapsed - 1 >= self.finish_time or self.stop_flag:
            self.stop_loop()
            return
        target_temp = self.target_temp(time_elapsed)
        self.pid.setpoint = target_temp

        real_temp = self.get_temperature()

        voltage_to_set = self.pid(real_temp)
        psu_voltage = self.set_new_voltage(voltage_to_set)

        psu_current = self.psu_channel.current

        data = "\t".join([str(x) for x in [time_elapsed, target_temp, real_temp,
                          psu_voltage, psu_current]])
        self.x.append(time_elapsed)
        self.y.append(real_temp)

        self.write_to_file(self.filename, data)
        self.plot_update.emit()

    def stop_loop(self):
        self.timer.stop()
        self.psu.output.off()
        print("Worker finished")
        self.finished.emit()

    def set_new_voltage(self, value):
        """This function sets new voltage on PSU, according to what
        PID result is"""
        self.psu_channel.voltage = value
        self.psu.output.on()
        return self.psu_channel.voltage

    def get_temperature(self):
        """Get a read from ADC and convert voltage value to temperature"""
        if self.adc_type == 'daqhat':
            voltage_read = self.adc.a_in_read(self.adc.CHANNEL_TEMPERATURE)
        else:
            voltage_read = adafruit.read.voltage
        # read in V, thermocouples needs mV
        return self.thermocouple.inverse_CmV(voltage_read * 1000, Tref=23)

    def get_config(self):
        config = ConfigParser(allow_no_value=True)
        config.read('config.ini')
        return config

    def initialize_loop_timer(self):
        """Function that initializes timer that is used to call main_loop"""
        timer = QTimer()
        timer.setInterval(500)
        timer.timeout.connect(self.main_loop)
        return timer

    def get_filename(self):
        """Create a full path  for performing operation"""
        date = datetime.strftime(datetime.now(), '%Y-%m-%d %H.%M.%S')
        filename = "RTA_log_{}".format(date)
        if self.config.has_option('DEFAULT', 'logpath'):
            path = os.path.join(self.config['DEFAULT']['logpath'])
        else:
            path = os.path.join(os.getcwd(), 'logs')
        full_path = os.path.join(path, filename)
        return full_path

    @staticmethod
    def write_to_file(path, data, mode='a'):
        if not os.path.isdir(os.path.split(path)[0]):
            os.mkdir(os.path.split(path)[0])
        with open(path, mode) as f:
            f.write("{}\n".format(data))

    def initialize_instruments(self):
        self.thermocouple = thermocouples['K']
        self.pid = self._init_pid()
        self.psu, self.psu_channel = self._init_psu()
        self.adc = self._init_adc()

    def _init_psu(self):
        psu_address = self.config['PSU']['address']
        device = psu.PSU(psu_address)
        device.over_voltage_protection.on()
        device.over_current_protection.off()
        device_channel = device.channels[0]
        device_channel.voltage = 0
        device_channel.current = 10
        return device, device_channel

    def _init_pid(self):
        # PID Controller
        Kp = float(self.config['PID']['P'])
        Ki = float(self.config['PID']['I'])
        Kd = float(self.config['PID']['D'])
        limits = (float(self.config['PID']['limit_lower']),
                  float(self.config['PID']['limit_upper'])
                  )
        PID = pid.PIDController(Kp, Ki, Kd)
        PID.output_limits = limits
        return PID

    def _init_adc(self):
        #DAQHAT HAS TOO LOW RESOLUTION FOR THERMOCOUPLE W/O AMPLIFIER
        if self.adc_type == 'daqhat':
        # try:
            daqhat_address = self.config.getint('DAQHAT', 'address')
            channel = self.config.getint('DAQHAT', 'channel_temperature')
            adc = daqhat.MCC118(daqhat_address)
            adc.CHANNEL_TEMPERATURE = channel
        # except daqhat.daqhats.HatError:
        else:
            adr = int(self.config['ADAFRUIT']['address'], 16)
            gain = self.config.getint('ADAFRUIT', 'gain')
            rate = self.config.getint('ADAFRUIT', 'data_rate')
            # adc = adafruit.ADC(address=adr, gain=gain, data_rate=rate)
            adc = adafruit.init_adc(adr, gain, rate)
        return adc
