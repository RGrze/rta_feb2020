from . import koradserial

class PSU(koradserial.KoradSerial):
    def __init__(self, port):
        super(PSU, self).__init__(port)
        self.over_current_protection.off()
        self.over_voltage_protection.on()
        self.channel = self.channels[0]
        self.channel.voltage = 0
        self.channel.current = 10
