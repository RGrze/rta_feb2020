from simple_pid import PID


class PIDController(PID):
    def __init__(self, P=0, I=0, D=0, setpoint=0, **kwargs):
        super(PIDController, self).__init__(P, I, D, setpoint, **kwargs)
