
import board
import busio
import adafruit_ads1x15.ads1115 as ADS
from adafruit_ads1x15.analog_in import AnalogIn


i2c = busio.I2C(board.SCL, board.SDA)

# ADC = ADS.ADS1115(i2c, address=0x49, data_rate=8)
# ADC.gain = gain
# read = AnalogIn(ADC, ADS.P0, ADS.P1)

def init_adc(adr, gain, rate):
    adc = ADS.ADS1115(i2c, address=adr, data_rate=rate)
    adc.gain = gain
    global read
    read = AnalogIn(adc, ADS.P0, ADS.P1)
    return adc
