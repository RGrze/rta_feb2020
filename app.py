#!/usr/bin/python3

import sys
import os

from PyQt5 import uic, QtGui
from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtWidgets import QMainWindow, QMessageBox, QFileDialog, QApplication, \
    QWidget

from recipe import Recipe
from worker import Worker


class MainApp(QMainWindow):
    def __init__(self):
        super(MainApp, self).__init__()
        uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                'gui.ui'), self)
        self.textBrowser_recipe.setReadOnly(True)

        self.recipe = Recipe()
        self.init_plotWidget()
        self._init_ui_signals()

    def resizeEvent(self, a0: QtGui.QResizeEvent) -> None:
        """Overrided default resize event that resizes plot widget according
        to the size of main window
        """
        main_window_old_height = a0.oldSize().height()
        main_window_old_width = a0.oldSize().width()
        main_window_new_height = a0.size().height()
        main_window_new_width = a0.size().width()
        # ignore resizing of the widget on app initialization
        if main_window_old_height == -1 or main_window_old_width == -1:
            return

        plot_widget_current_size = self.plotWidget.size()
        # get new width and height of plot widget
        width_diff = main_window_new_width - main_window_old_width
        plot_widget_new_width = plot_widget_current_size.width() + width_diff

        height_diff = main_window_new_height - main_window_old_height
        plot_widget_new_height = plot_widget_current_size.height() + height_diff

        self.plotWidget.resize(plot_widget_new_width, plot_widget_new_height)

    def init_plotWidget(self):
        xlabel = "Time, s"
        ylabel = "Temperature, C"
        self.plotWidget.getAxis('left').setLabel(ylabel)
        self.plotWidget.getAxis('right').setLabel(xlabel)
        self.plotWidget.showGrid(x=True, y=True)
        self.curve_real = self.plotWidget.plot(pen='b')
        self.curve_target = self.plotWidget.plot(pen='r')

    def _plot_on_data_arrive(self):
        self.curve_real.setData(self.worker.x, self.worker.y)

    def _init_ui_signals(self):
        self.pB_start.clicked.connect(self._on_start_button_clicked)

        self.pB_mod.clicked.connect(self._on_modify_recipe_button_clicked)
        self.pB_rec_new.clicked.connect(self.create_recipe)
        self.pB_rec_load.clicked.connect(lambda: self.load_recipe(caller='load_button'))
        self.pB_save.clicked.connect(self.save_recipe)

    def _on_start_button_clicked(self):
        if self.pB_start.text() == 'START':
            if self.recipe.isLoaded():
                self.thread, self.worker = self.create_worker()
                self.thread.start()
                self.pB_start.setText("STOP")
            else:
                self.show_warning_popup("Please select recipe first.")
        else:
            self.worker.stop_flag = True

    def create_worker(self):
        thread = QThread()
        worker = Worker(self.recipe)
        worker.moveToThread(thread)

        thread.started.connect(worker.run)
        worker.plot_update.connect(self._plot_on_data_arrive)
        worker.finished.connect(self._on_worker_finish)
        worker.finished.connect(thread.quit)
        worker.finished.connect(worker.deleteLater)
        thread.finished.connect(thread.deleteLater)
        return thread, worker

    def _on_worker_finish(self):
        self.pB_start.setText("START")

    # RECIPE STUFF
    def create_recipe(self):
        self.recipe_popup = Popup_new_recipe()
        self.recipe_popup.show()
        self.recipe_popup.return_recipe.connect(self._on_recipe_receive)

    def _on_recipe_receive(self):
        self.recipe_popup.hide()
        to_save = "target_time\ttarget_temperature\n"
        for time, temperature in zip(self.recipe_popup.time,
                                     self.recipe_popup.temperature):
            to_save += "{}\t{}\n".format(time, temperature)
        with open(self.recipe_popup.name, 'w') as f:
            f.write(to_save)
        self.load_recipe('popup')

    def load_recipe(self, caller):
        if caller == 'load_button':
            recipe_path = QFileDialog.getOpenFileName(None, 'Select file',
                                os.path.split(__file__)[0],
                                                      'Text files (*.csv *.txt)')[0]
        elif caller == 'save_button':
            recipe_path = self.recipe.path
        else:
            recipe_path = os.path.join(os.getcwd(), self.recipe_popup.name)
        if recipe_path:
            try:
                self.recipe(recipe_path)
            except ValueError:
                print(f"Couldn't load recipe ({recipe_path})")
                return
            self.label_recipe.setText(os.path.split(recipe_path)[1])
            self.fill_recipe_browser(self.recipe)
            self.curve_target.setData(self.recipe.time, self.recipe.temperature)

    def _on_modify_recipe_button_clicked(self):
        self.textBrowser_recipe.setReadOnly(False)

    def save_recipe(self):
        self.textBrowser_recipe.setReadOnly(True)
        lines = self.textBrowser_recipe.toPlainText().split('\n')
        target_time, target_temp = [], []
        for line in lines[1:]:
            if not line:
                continue
            _time, _temperature = line.split('\t')
            target_time.append(_time)
            target_temp.append(_temperature)
        self.recipe.save(self.recipe.path, target_time, target_temp)
        self.recipe(self.recipe.path)
        self.load_recipe('save_button')

    def fill_recipe_browser(self, recipe):
        text = "target_time\ttarget_temperature\n"
        for time, temperature in zip(recipe.time, recipe.temperature):
            text += "{}\t{}\n".format(time, temperature)
        self.textBrowser_recipe.clear()
        self.textBrowser_recipe.setText(text)
    ##############

    def show_warning_popup(self, text: str):
        """
        Pop up warning message box with customized text to prevent starting the measurement
        """
        warning = QMessageBox()
        warning.setText(text)
        warning.setStandardButtons(QMessageBox.Ok)
        warning.setIcon(QMessageBox.Warning)
        warning.exec()

class Popup_new_recipe(QWidget):
    return_recipe = pyqtSignal()
    def __init__(self):
        super(Popup_new_recipe, self).__init__()
        uic.loadUi(os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                'popup.ui'), self)
        self.pB_OK.clicked.connect(self._return_recipe)
        self.hide()

    def _return_recipe(self):
        self.time = [float(x) for x in self.lE_time.text().split(',')]
        self.temperature = [float(x) for x in self.lE_temperature.text().split(',')]
        self.name = self.lE_name.text()
        self.return_recipe.emit()
        self.hide()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainApp()
    window.show()
    sys.exit(app.exec_())
