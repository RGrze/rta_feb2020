class Recipe:
    def __init__(self):
        self.loaded = False

    def __call__(self, path):
        self.path = path
        self.time, self.temperature = self.load(self.path)

    def isLoaded(self):
        return self.loaded

    def load(self, path):
        temperature = []
        time = []
        with open(path, 'r') as f:
            # strip empty lines:
            lines = filter(None, f.read().splitlines()[1:])
            for i in lines:
                target_time, target_temp = i.split('\t')
                time.append(float(target_time))
                temperature.append(float(target_temp))
        self.loaded = True
        return time, temperature

    def save(self, file_path, data_time, data_temperature):
        with open(file_path, 'w') as f:
            f.write('target_time\ttarget_temp\n')
            for i in range(len(data_time)):
                f.write("{}\t{}\n".format(data_time[i], data_temperature[i]))
